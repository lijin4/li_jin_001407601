/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
public class Supplier {
    String supplierName="default supplicer name";
    String supplierAddress="default supplier address";
    long supplierNumber=0;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierAddress() {
        return supplierAddress;
    }

    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    public long getSupplierNumber() {
        return supplierNumber;
    }

    public void setSupplierNumber(long supplierNumber) {
        this.supplierNumber = supplierNumber;
    }
    

    
    
}
