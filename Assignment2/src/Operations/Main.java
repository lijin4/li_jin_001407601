/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operations;

import Business.Product;
import java.util.Scanner;

/**
 *
 * @author lenovo
 */
public class Main {
public static void main(String[] args) {
    Product p=new Product();
    System.out.println("Product Informations:"+"\n");
    System.out.println("Name:"+p.getName());
    System.out.println("Price:"+p.getPrice());
    System.out.println("Availabity Number:"+p.getAvailNum());
    System.out.println("Description:"+p.getDescription());
    System.out.println("SupplierName:"+p.getSupplier().getSupplierName());
    System.out.println("SupplierAddress:"+p.getSupplier().getSupplierAddress());
    System.out.println("SupplierNumber:"+p.getSupplier().getSupplierNumber()+"\n");
    System.out.println("---------------------------------------------------------");
    int option;
    Scanner scanner=new Scanner(System.in);
    CreatePro createpro=new CreatePro();
    ViewPro viewpro=new ViewPro();
    UpdatePro updatepro=new UpdatePro();
    
    while(true){
    try{
    System.out.println("Input numbers to choose the function:"+"\n"+"Input 1 to Create the ProductInfo;"+"\n"+"Input 2 to View the ProductInfo"+"\n"+"Input 3 to Update the ProductInfo"+"\n"+"Input 4 to Exit.");
    System.out.println("---------------------------------------------------------");
    option=Integer.parseInt(scanner.next());
    if(option!=1&&option!=2&&option!=3&&option!=4){
        System.out.println("Please input the option number!"+"\n");
    }else{
    switch(option){
        case 1: p=createpro.Create(p); break;
        case 2: p=viewpro.View(p);break;
        case 3: p=updatepro.Update(p);break;
    }     
    if(option==4){
        System.out.println("You've already exit the system!");
        break;
    }
    }
    
    }catch(Exception e){
        System.out.println("Wrong Type!!!"+"\n");
            }    
  }
 }
}

  
    

