/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operations;

import Business.Product;

/**
 *
 * @author lenovo
 */
public class ViewPro {
    
    Product product=new Product();
    public Product View(Product product){
        System.out.println("View the product information:");
        String name=product.getName();
        int price=product.getPrice();
        int availNum=product.getAvailNum();
        String des=product.getDescription();
        String supName=product.getSupplier().getSupplierName();
        String supAddress=product.getSupplier().getSupplierAddress();
        long supNum=product.getSupplier().getSupplierNumber();
        
        System.out.println("Name:"+name);
        System.out.println("Price:"+price);
        System.out.println("Available Number:"+availNum);
        System.out.println("Description:"+des);
        System.out.println("Supplier Name:"+supName);
        System.out.println("Supplier Address:"+supAddress);
        System.out.println("Supplier Number:"+supNum);
        System.out.println("---------------------------------------------------------");
        return product; 
    }
    
}
