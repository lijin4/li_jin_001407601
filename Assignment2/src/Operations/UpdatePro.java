package Operations;

import Business.Product;
import java.util.Scanner;


public class UpdatePro {
    public Product Update(Product product){
    Scanner scanner=new Scanner(System.in);
   
    System.out.println("Product default Informations:"+"\n");
    System.out.println("Name:"+product.getName());
    System.out.println("Price:"+product.getPrice());
    System.out.println("Availabity Number:"+product.getAvailNum());
    System.out.println("Description:"+product.getDescription());
    System.out.println("SupplierName:"+product.getSupplier().getSupplierName());
    System.out.println("SupplierAddress:"+product.getSupplier().getSupplierAddress());
    System.out.println("SupplierNumber:"+product.getSupplier().getSupplierNumber()+"\n"+"\n");
    System.out.println("Update the product informations:");
    System.out.println("You can exit by inputting 0."+"\n");
//   System.out.println("Please input the new name of the product:");
//System.out.println("Please input the number to choose the item that you want to update: 1.name;2.price;3.available number;4.description;5.supname;6.supadd;7.supnum");   

while(true){
   System.out.println("Input the number to choose the item that you want to update:"+"\n"+"1.name;"+"\n"+"2.price;"+"\n"+"3.available number;"+"\n"+"4.description;"+"\n"+"5.supname;"+"\n"+"6.supadd;"+"\n"+"7.supnum;"+"\n"+"0.exit");
   try{
   String a=scanner.next();
   int choice=Integer.parseInt(a);
   if(choice==0){
       break;
   }else if(choice!=1&&choice!=2&&choice!=3&&choice!=4&&choice!=5&&choice!=6&&choice!=7){
       System.out.println("Wrong number!!!!!!!!!!!"+"\n");
   }else{
        switch(choice){
        case 1:
            System.out.println("Please input the new name of the product:");
            product.setName(scanner.next());
            System.out.println("Informations are updated!:");
            System.out.println("---------------------------------------------------------");
            break;
        case 2: 
            while(true){
                    try{
                        System.out.println("Please input the new price of the product:");
                        int price=Integer.parseInt(scanner.next());
                        product.setPrice(price);
                        System.out.println("Informations are updated!");
                        System.out.println("---------------------------------------------------------");
                        break; 
                        }catch(Exception e){
                        System.out.println("***The information of new price should be a Number.***"+"\n");
                                            }
                        }
            continue;          
        case 3:     
            while(true){
                    try{
                        System.out.println("Please input the new available number of the product:");
                        int avainum=Integer.parseInt(scanner.next());
                        product.setAvailNum(avainum);
                        System.out.println("Informations are updated!");
                        System.out.println("---------------------------------------------------------");
                        break; 
                        }catch(Exception e){
                        System.out.println("***The information of new available number should be a Number.***"+"\n");
                                            }
                        }
            continue;
        case 4:     
            System.out.println("Please input the new description of the product:");
            product.setDescription(scanner.next());
            System.out.println("Informations are updated!:");
            System.out.println("---------------------------------------------------------");
            break;
        case 5:     
            System.out.println("Please input the new supplier name of the product:");
            product.getSupplier().setSupplierName(scanner.next());
            System.out.println("Informations are updated!:");
            System.out.println("---------------------------------------------------------");
            break;
        case 6:     
            System.out.println("Please input the new supplier address of the product:");
            product.getSupplier().setSupplierAddress(scanner.next());
            System.out.println("Informations are updated!:");
            System.out.println("---------------------------------------------------------");
            break;
        case 7:
            while(true){
                    try{
                        System.out.println("Please input the new supplier number of the product:");
                        long supnum=Long.parseLong(scanner.next());
                        product.getSupplier().setSupplierNumber(supnum);
                        System.out.println("Informations are updated!");
                        System.out.println("---------------------------------------------------------");
                        break; 
                        }catch(Exception e){
                        System.out.println("***The information of new supplier number should be a Number.***"+"\n");
                                            }
                        }
            continue;
       }
  }
 }catch(Exception e){
     System.out.println("Wrong Type!!!"+"\n");
  }
            }
   return product;
  }
}
