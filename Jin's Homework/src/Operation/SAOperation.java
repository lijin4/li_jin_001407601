/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

/**
 *
 * @author LENOVO
 */
public class SAOperation {
    private String SBankName;
    private String SRoutingNum;
    private String SAccountNum;
    private String SAccountBal;
    private String SAccountType;

    public String getSBankName() {
        return SBankName;
    }

    public void setSBankName(String SBankName) {
        this.SBankName = SBankName;
    }

    public String getSRoutingNum() {
        return SRoutingNum;
    }

    public void setSRoutingNum(String SRoutingNum) {
        this.SRoutingNum = SRoutingNum;
    }

    public String getSAccountNum() {
        return SAccountNum;
    }

    public void setSAccountNum(String SAccountNum) {
        this.SAccountNum = SAccountNum;
    }

    public String getSAccountBal() {
        return SAccountBal;
    }

    public void setSAccountBal(String SAccountBal) {
        this.SAccountBal = SAccountBal;
    }

    public String getSAccountType() {
        return SAccountType;
    }

    public void setSAccountType(String SAccountType) {
        this.SAccountType = SAccountType;
    }
    
}
