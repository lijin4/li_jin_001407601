/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

/**
 *
 * @author LENOVO
 */
public class CAOperation {
    private String CBankName;
    private String CRoutingNum;
    private String CAccountNum;
    private String CAccountBal;
    private String CAccountType;

    public String getCBankName() {
        return CBankName;
    }

    public void setCBankName(String CBankName) {
        this.CBankName = CBankName;
    }

    public String getCRoutingNum() {
        return CRoutingNum;
    }

    public void setCRoutingNum(String CRoutingNum) {
        this.CRoutingNum = CRoutingNum;
    }

    public String getCAccountNum() {
        return CAccountNum;
    }

    public void setCAccountNum(String CAccountNum) {
        this.CAccountNum = CAccountNum;
    }

    public String getCAccountBal() {
        return CAccountBal;
    }

    public void setCAccountBal(String CAccountBal) {
        this.CAccountBal = CAccountBal;
    }

    public String getCAccountType() {
        return CAccountType;
    }

    public void setCAccountType(String CAccountType) {
        this.CAccountType = CAccountType;
    }
    
}
