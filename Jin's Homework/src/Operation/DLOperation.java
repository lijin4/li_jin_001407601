/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

/**
 *
 * @author LENOVO
 */
public class DLOperation {
    private String LicenseNum;
    private String IssueDate;
    private String ExpirateDate;
    private String BloodType;
    private String Picture;

    public String getLicenseNum() {
        return LicenseNum;
    }

    public void setLicenseNum(String LicenseNum) {
        this.LicenseNum = LicenseNum;
    }

    public String getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(String IssueDate) {
        this.IssueDate = IssueDate;
    }

    public String getExpirateDate() {
        return ExpirateDate;
    }

    public void setExpirateDate(String ExpirateDate) {
        this.ExpirateDate = ExpirateDate;
    }

    public String getBloodType() {
        return BloodType;
    }

    public void setBloodType(String BloodType) {
        this.BloodType = BloodType;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String Picture) {
        this.Picture = Picture;
    }
    
}
