/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;
public class MainOperation {
    private DOperation DOperation;
    private AOperation AOeration;
    private SAOperation SAOperation;
    private CAOperation CAOperation;
    private DLOperation DLOperation;
    private MROperation MROperation;

    public MainOperation(){
           DOperation DOperation = new DOperation();
           AOperation AOeration = new  AOperation();
           SAOperation SAOeration = new  SAOperation();
           CAOperation CAOeration = new  CAOperation();
           DLOperation DLOperation= new DLOperation();
           MROperation MROperation= new MROperation();
    }
    
    public DOperation getDOperation() {
        return DOperation;
    }

    public void setDOperation(DOperation DOperation) {
        this.DOperation = DOperation;
    }

    public AOperation getAOeration() {
        return AOeration;
    }

    public void setAOeration(AOperation AOeration) {
        this.AOeration = AOeration;
    }

    public SAOperation getSAOperation() {
        return SAOperation;
    }

    public void setSAOperation(SAOperation SAOperation) {
        this.SAOperation = SAOperation;
    }

    public CAOperation getCAOperation() {
        return CAOperation;
    }

    public void setCAOperation(CAOperation CAOperation) {
        this.CAOperation = CAOperation;
    }

    public DLOperation getDLOperation() {
        return DLOperation;
    }

    public void setDLOperation(DLOperation DLOperation) {
        this.DLOperation = DLOperation;
    }

    public MROperation getMROperation() {
        return MROperation;
    }

    public void setMROperation(MROperation MROperation) {
        this.MROperation = MROperation;
    }

    
    
}
