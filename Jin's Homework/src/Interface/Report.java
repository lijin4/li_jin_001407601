/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;
import Operation.MainOperation;
import javax.swing.ImageIcon;

/**
 *
 * @author LENOVO
 */
public class Report extends javax.swing.JPanel {

    /**
     * Creates new form Report
     */
    public Report(MainOperation mainoperation) {
        initComponents();
        displayDOperation(mainoperation);
    }
    public void displayDOperation(MainOperation mainoperation){
        String doperationFname=mainoperation.getDOperation().getFname();
        FnameTextField.setText(doperationFname);
        
        String doperationLname=mainoperation.getDOperation().getLname();
        LnameTextField.setText(doperationLname);
        
        String doperationAge=mainoperation.getDOperation().getAge();
        AgeTextField.setText(doperationAge);
        
        String doperationDate=mainoperation.getDOperation().getDateOfBirth();
        DateTextField.setText(doperationDate);
        
        String doperationHeight=mainoperation.getDOperation().getHeight();
        HeightTextField.setText(doperationHeight);
        
        String doperationWeight=mainoperation.getDOperation().getWeight();
        WeightTextField.setText(doperationWeight);
        
        String doperationPhone=mainoperation.getDOperation().getPhoneNum();
        PhoneNumTextField.setText(doperationPhone);
        
        String doperationSocial=mainoperation.getDOperation().getSocialNum();
        SocialTextField.setText(doperationSocial); 
        
        String aoperationCity=mainoperation.getAOeration().getCity();
        CityTextField.setText(aoperationCity);
        
        String aoperationStreetA=mainoperation.getAOeration().getStreetAddress();
        StreetATextField.setText(aoperationStreetA);
        
        String aoperationState=mainoperation.getAOeration().getState();
        StateTextField.setText(aoperationState);
        
        String aoperationZipCode=mainoperation.getAOeration().getZipcode();
        ZipCodeTextField.setText(aoperationZipCode);
        
        String caoperationbankname=mainoperation.getCAOperation().getCBankName();
        BanknameTextFieldc.setText(caoperationbankname);
        
        String caoperationRoutingNum=mainoperation.getCAOperation().getCRoutingNum();
        RoutingNumTextFieldc.setText(caoperationRoutingNum);
        
        String caoperationAccountNum=mainoperation.getCAOperation().getCAccountNum();
        AccountNumTextFieldc.setText(caoperationAccountNum);
        
        String caoperationAccountBal=mainoperation.getCAOperation().getCAccountBal();
        AccountBalTextFieldc.setText(caoperationAccountBal);
        
        
        String dloperationLicenseNum=mainoperation.getDLOperation().getLicenseNum();
        LicenseNumRepTextField.setText(dloperationLicenseNum);
        
        String dloperationIssueDate=mainoperation.getDLOperation().getIssueDate();
        IssueDateRepTextField.setText(dloperationIssueDate);
        
        String dloperationBlood=mainoperation.getDLOperation().getBloodType();
        BloodRepTextField.setText(dloperationBlood);

        String dloperationExpirate=mainoperation.getDLOperation().getExpirateDate();
        ExpirateDateRepTextField1.setText(dloperationExpirate);
        
        String dloperationPic=mainoperation.getDLOperation().getPicture();
        PictureTextField_pathv.setText(dloperationPic);
        
        
        String filename=PictureTextField_pathv.getText();
        PictureTextField_pathv.setText(filename);
        ImageIcon icon=new ImageIcon(filename);
        icon.setImage(icon.getImage().getScaledInstance(180, 180,180));
        picUpdate.setIcon(icon);
        
        String mroperationMedRed=mainoperation.getMROperation().getMrecordNUM();
        MedRedTextField.setText(mroperationMedRed);
        
        String mroperationAlergy1=mainoperation.getMROperation().getAlergy1();
        Alergy1TextField.setText(mroperationAlergy1);
        
        String mroperationAlergy2=mainoperation.getMROperation().getAlergy2();
        Alergy2TextField.setText(mroperationAlergy2);
        
        String mroperationAlergy3=mainoperation.getMROperation().getAlergy3();
        Alergy3TextField.setText(mroperationAlergy3);
        
        String saoperationbankname=mainoperation.getSAOperation().getSBankName();
        BanknameTextField.setText(saoperationbankname);
        
        String saoperationRoutingNum=mainoperation.getSAOperation().getSRoutingNum();
        RoutingNumTextField.setText(saoperationRoutingNum);
        
        String saoperationAccountNum=mainoperation.getSAOperation().getSAccountNum();
        AccountNumTextField.setText(saoperationAccountNum);
        
        String saoperationAccountBal=mainoperation.getSAOperation().getSAccountBal();
        AccountBalTextField.setText(saoperationAccountBal);
        
          
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        LicenseNumRepTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        IssueDateRepTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        ExpirateDateRepTextField1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        BloodRepTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        FnameTextField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        LnameTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        PhoneNumTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        DateTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        AgeTextField = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        HeightTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        WeightTextField = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        SocialTextField = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        StreetATextField = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        CityTextField = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        StateTextField = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        ZipCodeTextField = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        MedRedTextField = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        Alergy1TextField = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        Alergy2TextField = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        Alergy3TextField = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        BanknameTextField = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        RoutingNumTextField = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        AccountNumTextField = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        AccountBalTextField = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        BanknameTextFieldc = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        AccountBalTextFieldc = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        RoutingNumTextFieldc = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        AccountNumTextFieldc = new javax.swing.JTextField();
        picUpdate = new javax.swing.JLabel();
        PictureTextField_pathv = new javax.swing.JTextField();

        jLabel1.setFont(new java.awt.Font("宋体", 0, 24)); // NOI18N
        jLabel1.setText("Personal Information Report");

        jLabel2.setText("Picture:");

        jLabel3.setText("Driver's LicenseNum:");

        jLabel4.setText("Driver's License IssueDate:");

        jLabel5.setText("Driver's License ExpirateDate:");

        jLabel6.setText("Driver's BloodType:");

        jLabel7.setText("FirstName:");

        FnameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FnameTextFieldActionPerformed(evt);
            }
        });

        jLabel8.setText("LastName:");

        jLabel9.setText("PhoneNumber:");

        jLabel10.setText("Date Of Birth:");

        jLabel11.setText("Age:");

        jLabel12.setText("Height:");

        jLabel13.setText("Weight:");

        jLabel14.setText("Social Security Number:");

        jLabel15.setText("StreetAddress:");

        StreetATextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StreetATextFieldActionPerformed(evt);
            }
        });

        jLabel16.setText("City:");

        jLabel17.setText("State:");

        jLabel18.setText("Zip Code:");

        jLabel19.setText("MedicalRecordNum:");

        MedRedTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MedRedTextFieldActionPerformed(evt);
            }
        });

        jLabel20.setText("Alergy1:");

        jLabel21.setText("Alergy2:");

        jLabel22.setText("Alergy3:");

        jLabel23.setFont(new java.awt.Font("宋体", 1, 18)); // NOI18N
        jLabel23.setText("Saving account information：");

        jLabel24.setFont(new java.awt.Font("宋体", 1, 18)); // NOI18N
        jLabel24.setText("Checking account information：");

        jLabel25.setText("Bank name:");

        BanknameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BanknameTextFieldActionPerformed(evt);
            }
        });

        jLabel26.setText("Bank Routing Number:");

        jLabel27.setText("Bank Account Number:");

        AccountNumTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccountNumTextFieldActionPerformed(evt);
            }
        });

        jLabel28.setText("Account Balance:");

        jLabel29.setText("Bank name:");

        BanknameTextFieldc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BanknameTextFieldcActionPerformed(evt);
            }
        });

        jLabel30.setText("Account Balance:");

        jLabel31.setText("Bank Routing Number:");

        jLabel32.setText("Bank Account Number:");

        AccountNumTextFieldc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccountNumTextFieldcActionPerformed(evt);
            }
        });

        picUpdate.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        PictureTextField_pathv.setDisabledTextColor(new java.awt.Color(240, 240, 240));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator3)
            .addComponent(jSeparator5)
            .addComponent(jSeparator1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(picUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LnameTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(FnameTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(PhoneNumTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(18, 18, 18)
                                .addComponent(HeightTextField))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(AgeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel13)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(WeightTextField))
                                    .addComponent(DateTextField)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(347, 347, 347)
                        .addComponent(jLabel1)))
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SocialTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(PictureTextField_pathv, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jSeparator2)
            .addComponent(jSeparator4)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel16))
                                .addGap(78, 78, 78)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(StreetATextField)
                                    .addComponent(CityTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel18))
                                .addGap(118, 118, 118)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(StateTextField)
                                    .addComponent(ZipCodeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(155, 155, 155)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(BloodRepTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ExpirateDateRepTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LicenseNumRepTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(IssueDateRepTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addGap(380, 380, 380)
                        .addComponent(jLabel24))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel27)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(AccountNumTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel26)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(RoutingNumTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(BanknameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39)
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(AccountBalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(66, 66, 66)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(BanknameTextFieldc, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(87, 87, 87)
                                .addComponent(jLabel30)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(AccountBalTextFieldc, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel32)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(AccountNumTextFieldc, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel31)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(RoutingNumTextFieldc, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel20))
                                .addGap(55, 55, 55)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(MedRedTextField)
                                    .addComponent(Alergy1TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addGap(127, 127, 127)
                                .addComponent(Alergy2TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(153, 153, 153)
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Alergy3TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(picUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(FnameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(LnameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(PhoneNumTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(DateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14)
                            .addComponent(SocialTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(AgeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(WeightTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PictureTextField_pathv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(HeightTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(22, 22, 22)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel15)
                                .addComponent(StreetATextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(CityTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(IssueDateRepTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(StateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(ExpirateDateRepTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(LicenseNumRepTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(ZipCodeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(BloodRepTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(MedRedTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Alergy3TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(Alergy1TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Alergy2TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21))))
                .addGap(26, 26, 26)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(jLabel24))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(BanknameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel28)
                            .addComponent(AccountBalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(RoutingNumTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel26))
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel27)
                            .addComponent(AccountNumTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel29)
                            .addComponent(BanknameTextFieldc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel30)
                            .addComponent(AccountBalTextFieldc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(RoutingNumTextFieldc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31))
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel32)
                            .addComponent(AccountNumTextFieldc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void FnameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FnameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FnameTextFieldActionPerformed

    private void StreetATextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StreetATextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_StreetATextFieldActionPerformed

    private void MedRedTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MedRedTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_MedRedTextFieldActionPerformed

    private void BanknameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BanknameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BanknameTextFieldActionPerformed

    private void AccountNumTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccountNumTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AccountNumTextFieldActionPerformed

    private void BanknameTextFieldcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BanknameTextFieldcActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BanknameTextFieldcActionPerformed

    private void AccountNumTextFieldcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccountNumTextFieldcActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AccountNumTextFieldcActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField AccountBalTextField;
    private javax.swing.JTextField AccountBalTextFieldc;
    private javax.swing.JTextField AccountNumTextField;
    private javax.swing.JTextField AccountNumTextFieldc;
    private javax.swing.JTextField AgeTextField;
    private javax.swing.JTextField Alergy1TextField;
    private javax.swing.JTextField Alergy2TextField;
    private javax.swing.JTextField Alergy3TextField;
    private javax.swing.JTextField BanknameTextField;
    private javax.swing.JTextField BanknameTextFieldc;
    private javax.swing.JTextField BloodRepTextField;
    private javax.swing.JTextField CityTextField;
    private javax.swing.JTextField DateTextField;
    private javax.swing.JTextField ExpirateDateRepTextField1;
    private javax.swing.JTextField FnameTextField;
    private javax.swing.JTextField HeightTextField;
    private javax.swing.JTextField IssueDateRepTextField;
    private javax.swing.JTextField LicenseNumRepTextField;
    private javax.swing.JTextField LnameTextField;
    private javax.swing.JTextField MedRedTextField;
    private javax.swing.JTextField PhoneNumTextField;
    private javax.swing.JTextField PictureTextField_pathv;
    private javax.swing.JTextField RoutingNumTextField;
    private javax.swing.JTextField RoutingNumTextFieldc;
    private javax.swing.JTextField SocialTextField;
    private javax.swing.JTextField StateTextField;
    private javax.swing.JTextField StreetATextField;
    private javax.swing.JTextField WeightTextField;
    private javax.swing.JTextField ZipCodeTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JLabel picUpdate;
    // End of variables declaration//GEN-END:variables
}
